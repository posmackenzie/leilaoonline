/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mackenzie.pis;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Fernando
 */

@Entity
@Table(name="LANCE")
public class Lance implements Serializable{

    @Id
    @Column(name="ID")
    private Long id;
    
    @ManyToOne
    @JoinColumn(columnDefinition = "ID_USUARIO", referencedColumnName = "ID")
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(columnDefinition = "ID_LEILAO", referencedColumnName = "ID")
    private Leilao leilao;
    
    @Column(name="DATA_LANCE")
    @Temporal(TemporalType.DATE)
    private Date dataLance;
    
    @Column(name="VALOR")
    private Double valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Date getDataLance() {
        return dataLance;
    }

    public void setDataLance(Date dataLance) {
        this.dataLance = dataLance;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    
    
    
}
