/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mackenzie.pis;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Fernando
 */
@Entity
@Table(name="USUARIO")
public class Usuario implements Serializable{
    
    @Id
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "NOME",nullable = false)
    private String nome;
    
    @Column(name = "LOGIN", unique = true)
    private String login;
    
    @Column(name = "SENHA", nullable = false)
    private String senha;
    
    @Email
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;
    
    @Column(name = "CPF", nullable = false, unique = true)
    private String cpf;
    
    @Column(name = "SITUACAO", nullable = false, unique = true)
    private String situacao;
    
    
    @Column(name = "TELEFONE")
    private Integer telefone;
    
    @Column(name = "ENDERECO")
    private String endereco;
    
    @ManyToMany
    @JoinTable(name = "USUARIO_LEILAO")
    private List<Leilao> leiloes;

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    
    
    
    
}
