/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mackenzie.pis;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Fernando
 */
@Entity
@Table(name="LOTE")
public class Lote implements Serializable{
    
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "SQ_LOTE")
    private Long id;
    
    @ManyToOne
    @JoinColumn(columnDefinition = "CRIADO_POR",referencedColumnName = "ID")
    private Administrador criadoPor;
    
    @ManyToOne
    @JoinColumn(columnDefinition = "CRIADO_POR",referencedColumnName = "ID")
    private Administrador atualizadoPor;
    
    @ManyToMany
    @JoinTable(name = "LOTE_PRODUTO")
    private List<Produto> produtos;
    
    @Column(name="DATA_CRIACAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCriacao;
    
    @Column(name="DATA_ATUALIZACAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAtualizacao;
    
    @Column(name="NUMERO")
    private Long numero;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Administrador getCriadoPor() {
        return criadoPor;
    }

    public void setCriadoPor(Administrador criadoPor) {
        this.criadoPor = criadoPor;
    }

    public Administrador getAtualizadoPor() {
        return atualizadoPor;
    }

    public void setAtualizadoPor(Administrador atualizadoPor) {
        this.atualizadoPor = atualizadoPor;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }
    
    
    
    
    
}
