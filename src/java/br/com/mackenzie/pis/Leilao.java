/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mackenzie.pis;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Fernando
 */
@Entity
@Table(name= "LEILAO")
public class Leilao implements Serializable{
    
    @Id
    @Column(name="ID")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="ID_LEILOEIRO", referencedColumnName = "ID")
    private Leiloeiro leiloeiro;
    
    @ManyToOne
    @JoinColumn(name="ID_LOTE", referencedColumnName = "ID")
    private Lote lote;
    
    @Column(name="INICIO_LEILAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date inicioLeilao;
    
    @Column(name="TERMINO_LEILAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date terminoLeilao;
    
    @Column(name="DATA_CRIACAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCriacao;
    
    @Column(name="VALOR_INICIAL")
    private Double valorInicial;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Leiloeiro getLeiloeiro() {
        return leiloeiro;
    }

    public void setLeiloeiro(Leiloeiro leiloeiro) {
        this.leiloeiro = leiloeiro;
    }

    public Lote getLote() {
        return lote;
    }

    public void setLote(Lote lote) {
        this.lote = lote;
    }

    public Date getInicioLeilao() {
        return inicioLeilao;
    }

    public void setInicioLeilao(Date inicioLeilao) {
        this.inicioLeilao = inicioLeilao;
    }

    public Date getTerminoLeilao() {
        return terminoLeilao;
    }

    public void setTerminoLeilao(Date terminoLeilao) {
        this.terminoLeilao = terminoLeilao;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }
    
    
    
}
